const net = require('net')
const config = require('../conf/main')

let hasServer = false
if(hasServer) return

const server = net.createServer({ allowHalfOpen: true }, socket => {
  socket.pipe(socket)
  // socket.on('data', socket.write)
}).on('error', err => console.error)

server.listen({
  host: 'localhost',
  port: config.proxyport,
  exclusive: true
})

hasServer = true
