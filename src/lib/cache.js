const Stream = require('stream')
class StreamCache extends Stream{
  constructor() {
    super()
    this._maxListeners = 250
    this._buffers = []
    this._dests = []
    this._ended = false
  }
  write(buffer) {
    this._buffers.push(buffer)
    for(let dest of this._dests) {
      dest.write(buffer)
    }
  }
  pipe(dest, options) {
    if(options) {
      throw new Error('Options is not supported //')
    }
    for(let buffer of this._buffers) {
      dest.write(buffer)
    }
    if(this._ended) {
      dest.end()
      return dest
    }
    this._dests.push(dest)
    return dest
  }
  getLength() {
    return this._buffers.reduce((totalLength, buffer) => {
      return totalLength + buffer.length
    }, 0)
  }
  end() {
    for(let dest of this._dests) {
      dest.end()
    }
    this._ended = true
    this.dests = []
    this.emit('finish')
  }
}
module.exports = class Cache {
  constructor() {
    this.cached = new Map()
  }
  get(key) {
    return this.cached.get(key)
  }
  set(key) {
    this.cached.delete(key)
    this.cached.set(key, new StreamCache())
    return this.cached.get(key)
  }
}
