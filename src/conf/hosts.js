
const hosts = [
  {
    host: 'localhost',
    originsWithAccess: [
      'localhost:3000'
    ],
    ssl: false,
    letsencrypt: false,
    onload: ['api/test.js'],
    loader: async ({ path, connection }) => {
      // This bit is used for SPA. One file for all connections.
      //
      // if(path.startsWith('/ourapp') && !path.includes('.')) {
      //   await connection.renderStatic('/build/index.html')
      //   return true
      // }

      // This bit is used for getting files via some path
      //
      // else if(path.startsWith('/storage')) {
      //   const { requestPath } = connection
      //   const [ fileName ] = requestPath.split('/').slice(-1)
      //   const [ file ] = fileName.split('.')
      //   connection.data = connection.data || {}
      //   connection.data.file = file
      //   connection.requestPath = '/api/chat/getFile'
      // }
      
    }
  },
]

module.exports = hosts