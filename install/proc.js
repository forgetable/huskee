#!/usr/bin/env node
const fs = require('fs-extra')
const userPath = process.cwd()
const path = require('path')

fs.copySync(path.join(__dirname, '../src'), `${userPath}/huskee`)
